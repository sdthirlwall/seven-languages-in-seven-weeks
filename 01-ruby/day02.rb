#!/usr/bin/env ruby

# Print the contents of an array of sixteen numbers, four numbers at a time,
# using just each. Now, do the same with each_slice in Enumerable.

numbers = (11..26).to_a

slice = [ ]
numbers.each do |n|
    slice.push(n)
    if slice.length == 4 then
        puts slice.join(" ")
        slice = [ ]
    end
end

numbers.each_slice(4) { |slice| puts slice.join(" ") }

# The Tree class was interesting, but it did not allow you to specify a new tree
# with a clean user interface. Let the initializer accept a nested structure
# with hashes and arrays. You should be able to specify a tree like this:
# {'grandpa' => { 'dad' => {'child 1' => {}, 'child 2' => {} }, 'uncle' => {'child 3' => {}, 'child 4' => {} } } }

class Tree
    attr_accessor :children, :name

    def initialize(spec)
        @name = spec.keys[0]
        @children = []
        spec[@name].each do |name, children|
            @children.push(Tree.new({ name => children }))
        end
    end

    def visit_all(&block)
        visit &block
        children.each {|c| c.visit_all &block}
    end

    def visit(&block)
        block.call self
    end
end

treeSpec = {
    'grandpa' => {
        'dad' => {
            'child 1' => {},
            'child 2' => {},
        },
        'uncle' => {
            'child 3' => {},
            'child 4' => {},
        },
    },
}

tree = Tree.new(treeSpec)
tree.visit_all {|node| puts node.name}
