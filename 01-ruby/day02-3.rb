#!/usr/bin/env ruby

if ARGV.length != 2 then
    puts "usage: #{ $0 } pattern file"
    exit
end

pattern  = ARGV[0]
filename = ARGV[1]
re = Regexp.new(pattern)

File.open(filename).each_with_index do |line, index|
    if re =~ line then
        puts "#{ index + 1 }: #{ line }"
    end
end
