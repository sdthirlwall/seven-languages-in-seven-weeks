#!/usr/bin/env ruby

# Print the string “Hello, world.”
puts "Hello, world."

# For the string “Hello, Ruby,” find the index of the word “Ruby.”
index = "Hello, Ruby".index("Ruby")

# Print your name ten times.
myName = 'my name'
for i in 1 .. 10
    puts myName
end
(1 .. 10).each do
    puts myName
end

# Print the string “This is sentence number 1,” where the number 1 changes from 1 to 10.
(1 .. 10).each { |n| puts "This is sentence number #{ n }" }
(1 .. 10).each do |n|
    puts "This is sentence number #{ n }"
end
